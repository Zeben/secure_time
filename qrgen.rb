#!/usr/bin/env ruby
require 'rubyserial'
require 'rqrcode'
serial = Serial.new '/dev/ttyUSB1', 57600
args = ARGV[0]
hid = "FSCiQVn4oWamCtCUBq4Z8gtt!"


qr = RQRCode::QRCode.new(args, size: 3, level: :l)
qrsymbols = ""
qcache = "00000000000000000000000000000000000000000000000000000000000000000000000000000000,"
counter = 1
sleep(1)
serial.write(hid)
sleep(0.1)
serial.write(qcache)
sleep(0.1)
serial.write(qcache)
sleep(0.1)
serial.write(qcache)
sleep(0.1)
qr.modules.each do |c|
  qrsymbols << "000000000000000000000000000000000"
  c.each do |r|
    if r == true
      qrsymbols << "11"
    else
      qrsymbols << "00"
    end
  end
  qrsymbols << "000000000000000000000000000000000"
  qrsymbols << ','
  serial.write(qrsymbols)
  sleep(0.01)
  serial.write(qrsymbols)
  sleep(0.01)
  print qrsymbols
  puts " Row: #{counter}, Length: #{qrsymbols.length}"
  qrsymbols.clear
  counter = counter + 1
  # sleep(0.002)
end
sleep(0.01)
serial.write(qcache)
sleep(0.01)
serial.write(qcache)
sleep(0.01)
serial.write(qcache)
sleep(0.01)
serial.write("\n")
