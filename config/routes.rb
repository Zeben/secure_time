Rails.application.routes.draw do
  get 'dashboards/index'
  root 'dashboards#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/login' => 'sessions#new', as: 'login'
  get '/logout' => 'sessions#destroy', as: 'logout'
  post '/intro/:id' => 'users#checkpoint_intro', as: 'intro'
  post '/outro/:id' => 'users#checkpoint_outro', as: 'outro'

  resources :sessions

  resources :users do
    resources :devices
  end

  resources :devices do
    resources :users
  end

  resources :au, only: :index
  get '/qr' => 'au#render_qr_code', as: 'render_qr_code'


  # get '*path' => redirect('/404')
end
