#include <OLED_I2C.h>
OLED  ol(SDA, SCL, 8);
extern uint8_t TinyFont[];
String hid = "FSCiQVn4oWamCtCUBq4Z8gtt";
String given_hid;
bool hid_flag = true;

void setup() {
  Serial.begin(57600);
  ol.begin();
  pinMode(13, OUTPUT);
}

int cX = 1;
int cY = 1;
void loop() {
  while(Serial.available())
  {
    ++cX;
    char c = Serial.read();
    if(c == '!') 
    {
      hid_flag = false;
      if(hid == given_hid)
      {
        Serial.println(given_hid);
        digitalWrite(13, HIGH);  
      }
    }
    if(hid_flag) 
    {
      given_hid += c;
    }
    if(c == '0') ol.setPixel(cX, cY);
    if(c == ',')
    {
      ++cY;  
      cX = 1;
    }
    
    if(c == '\n')
    {
      ol.setPixel(2,2);
      ol.print(given_hid, CENTER, 0);
      ol.update();
      cX = 1;  
    }
    
    if(c == ';')
    {
      Serial.print("clear");
      ol.clrScr(); 
      cX = 1;
      cY = 1; 
    }
  }
  
}
