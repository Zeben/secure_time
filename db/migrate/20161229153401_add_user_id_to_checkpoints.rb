class AddUserIdToCheckpoints < ActiveRecord::Migration[5.0]
  def change
    add_column :checkpoints, :user_id, :integer
  end
end
