class CreateDevices < ActiveRecord::Migration[5.0]
  def change
    create_table :devices do |t|
      t.string :hid
      t.string :randomid
      t.string :description
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
