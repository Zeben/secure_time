class AddForeignKeyToUsers < ActiveRecord::Migration[5.0]
  def change
    add_reference :users, :devices, foreign_key: true
  end
end
