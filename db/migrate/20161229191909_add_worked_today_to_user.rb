class AddWorkedTodayToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :worked_today, :string
  end
end
