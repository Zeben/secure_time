class User < ApplicationRecord
  include PgSearch
  pg_search_scope :search_users, against: :name,
                  :using => {
                      :tsearch => {:prefix => true}
                  }
  rolify
  has_secure_password
  has_many :checkpoints, dependent: :destroy
  has_and_belongs_to_many :devices
  validates :name, presence: true, uniqueness: true
  validates :nickname, presence: true
  validates :password, presence: true, if: lambda { new_record? || !password.blank? }
  validates :password, presence: true, on: create

  private

  def self.toUnixTime(timeString)
    return DateTime.parse(timeString).to_time.to_i
  end

  def self.total_hours_today(checkpoints_object)
    diff = 0
    flag = ''
    seen = DateTime.now.to_time.to_i
    checkpoints = checkpoints_object.where('DATE(created_at) = ?', Date.today)
    checkpoints.reverse_each do |checkpoint|
      if flag == 'outro'
        diff += (seen - toUnixTime(checkpoint.created_at.to_s))
      end
      flag = checkpoint.status
      seen = toUnixTime(checkpoint.created_at.to_s)
    end
    return Time.at(diff).utc.strftime('%H:%M:%S')
  end
end
