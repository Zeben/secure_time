class Device < ApplicationRecord
  has_and_belongs_to_many :users
  validates :hid, length: { is: 24 }
  validates :hid, presence: true, uniqueness: true
  accepts_nested_attributes_for :users, allow_destroy: true
end
