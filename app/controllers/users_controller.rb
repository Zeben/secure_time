class UsersController < ApplicationController
  before_action :deny, except: :show

  def index
    @users = User.includes(:roles).order(updated_at: :desc)
    @page_title = '- Все пользователи'
  end

  def new
    @page_title = '- Добавить пользователя'
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      @user.add_role params[:role][:role_name]
      redirect_to users_path, notice: 'Новый пользователь в сети'
    else
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update(user_params)
      redirect_to users_path
    else
      render 'new'
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy
    redirect_to users_path
  end

  def checkpoint_intro
    @user = User.find(params[:id])
    @user.online = true
    @user.save
    @checkpoint = @user.checkpoints.new
    @checkpoint.status = 'intro'
    if @checkpoint.save
      redirect_to '/', notice: "Сотрудник #{@user.nickname} вошёл на предприятие в #{@checkpoint.created_at}"
    end
  end

  def checkpoint_outro
    @user = User.find(params[:id])
    @user.online = false
    @user.save
    @checkpoint = @user.checkpoints.new
    @checkpoint.status = 'outro'
    if @checkpoint.save
      redirect_to '/', notice: "Сотрудник #{@user.nickname} вышел из предприятия в #{@checkpoint.created_at}"
    end
  end

  def show
    @user = User.find(params[:id])
    @checkpoints = @user.checkpoints.all
  end

  private

  def user_params
    params.require(:user).permit(:name, :password, :password_confirmation, :nickname)
  end
end
