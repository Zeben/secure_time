class DashboardsController < ApplicationController
  def index
    @page_title = '- Главная страница'
    if current_user
      redirect_to user_path(current_user.id) if current_user.has_role? :user
    else
      redirect_to '/login'
    end
    @results = User.search_users(params[:search_input])
    @last_changed_users = User.order(updated_at: :desc).limit(5)
    respond_to do |format|
      format.html
      format.js
    end
  end
end
