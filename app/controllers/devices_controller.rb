class DevicesController < ApplicationController
  before_action :deny

  def index
    @page_title = '- Контроллеры'
    @devices = Device.all
  end

  def new
    @page_title = '- Авторизировать контроллер'
    @device = Device.new
    @users = User.all
  end

  def create
    @device = Device.new(device_params)
    @device.users << User.find(params[:device][:user_id])
    @device.randomid = generate_api_token
    if @device.save
      redirect_to devices_path, notice: 'Контроллер авторизирован'
    else
      render 'new'
    end
  end

  def edit
    @page_title = '- Редактировать данные контроллера'
    @device = Device.find(params[:id])
    @users = @device.users.all
  end

  def update
    @device = Device.find(params[:id])
    @device.users << User.find(params[:device][:user_id])
    if @device.update(device_params)
      redirect_to devices_path
    else
      render 'new'
    end
  end

  def destroy
    if !params[:user_id].nil?
      @device = Device.find(params[:id])
      @user = @device.users.find(params[:user_id])
      if @device.users.destroy(@user)
        redirect_to edit_device_path @device.id
      end
    else
      @device = Device.find(params[:id])
      if @device.destroy
        redirect_to devices_path, notice: 'Контроллер удалён'
      end
    end
  end

  private

  def device_params
    params.require(:device).permit(:hid, :user, :user_id)
  end

end
