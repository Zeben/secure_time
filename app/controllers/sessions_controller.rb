class SessionsController < ApplicationController

  layout 'login'

  def index
    redirect_to root_url
  end

  def new
    redirect_to root_url if current_user
  end

  def create
    user = User.find_by(name: params[:name])
    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to root_url, notice: 'Авторизация успешна'
    else
      flash.now.alert = 'Что-то пошло не так, повторите'
      render 'new'
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url
  end
end
