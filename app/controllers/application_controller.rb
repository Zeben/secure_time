class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  helper_method :current_user,:generate_api_token

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def auth
    redirect_to root_url unless current_user
  end

  def admin?
    true if current_user && current_user.has_role?(:admin)
  end

  def deny
    unless admin?
      redirect_to root_url, notice: 'Только администраторы могут получить доступ к этому разделу'
    end
  end

  def generate_api_token
    loop do
      token = SecureRandom.base64.tr('+/=', 'Qrt')
      break token if Device.find_by(randomid: token).nil?
    end
  end
end
