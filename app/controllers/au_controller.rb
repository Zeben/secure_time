class AuController < ApplicationController
  before_action :auth
  def index
    begin
      # открываем порт на 9600
      s = Serial.new '/dev/ttyUSB0', 57600

      given_rid = params[:rid]

      # находим контроллер по RID, который был зашифрован в QR-коде
      device = Device.includes(:users).find_by_randomid(given_rid)

      # генерируем токен
      rid_token = generate_api_token

      # сразу меняем RID, чтобы предотвратить любой несанционированный доступ
      device.randomid = rid_token

      # сразу проводим транзакцию на смену RID
      device.save!

      # находим пользователя, который попытался получить доступ к двери, через его сессию
      user = device.users.find(current_user.id)


      # отправляем по UART HID контроллера, чтобы тот отозвался
      # s.write("#{device.hid}#{rid_token};")
      sleep(1)
      s.write("#{device.hid}!")
      send_qr_uart s, "#{get_ip_addr}:3000/au?rid=#{rid_token}"
      # s.write("!#{device.hid}.")

      ### здесь отправляем команду на открытие двери в SerialPort

      # если пользователь привязан к контроллеру - открываем дверь
      if user.roles.first.name == 'admin'
        render json: { success: "Door opened for DEVICE==USER=#{user.name}. New token has been generated",
                     code: "#{device.hid}#{rid_token}" }
      else
        redirect_to root_url
      end
      

    rescue Exception => e
      render json: { error: "Internal error or incorrect token: Door is't opened.", explanation: e }
    end
  end

  def render_qr_code


    rid = Device.first.randomid
    @page_title = '- QR код тестового контроллера'
    @line = "#{get_ip_addr}:3000/au/?rid=#{rid}"
    @qrcode = RQRCode::QRCode.new(@line, size: 3, level: :l)
    respond_to do |format|
      format.js
      format.html
    end
  end

  private

  def generate_qr_for_all_controllers

  end

  def send_qr_uart serial_obj, str
    qr = RQRCode::QRCode.new(str, size: 3, level: :l)
    qrsymbols = ""
    qcache = ''
    127.times { qcache += '0' }
    counter = 1
    sleep(0.1)
    serial_obj.write(qcache)
    sleep(0.1)
    serial_obj.write(qcache)
    sleep(0.1)
    puts qcache
    # serial_obj.write(qcache)
    # sleep(0.1)
    qr.modules.each do |c|
      qrsymbols << "000000000000000000000000000000000" # 33 times before qr-code creation
      c.each do |r|
        if r == true
          qrsymbols << "11"
        else
          qrsymbols << "00"
        end
      end
      qrsymbols << "000000000000000000000000000000000"# 33 times after qr-code creation
      qrsymbols << ','
      serial_obj.write(qrsymbols)
      sleep(0.01)
      serial_obj.write(qrsymbols)
      puts qrsymbols
      sleep(0.01)
      # print qrsymbols
      # puts " Row: #{counter}, Length: #{qrsymbols.length}"
      qrsymbols.clear
      counter = counter + 1
      # sleep(0.002)
    end
    sleep(0.01)
    serial_obj.write(qcache)
    sleep(0.01)
    serial_obj.write(qcache)
    sleep(0.01)
    serial_obj.write(qcache)
    sleep(0.01)

    serial_obj.write("\n")
    puts qcache
    sleep(0.1)
  end

  def get_ip_addr
    ip_addr = `ip a | grep 192 | cut -d " " -f 6 | cut -d '/' -f 1`.gsub("\n", "")
    if ip_addr.empty?
      ip_addr = '[no_ip_address]'
    end
    ip_addr
  end

end
